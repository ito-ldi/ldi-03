/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package lesm.tamanioAlmacenamiento;

import java.util.Scanner;

/**
 *
 * @author Luis
 */
public class TipoByte{ 
    private static final String[] tiposDeDatos = {"Byte","Word","DWord","QWord"};
    public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
    }
    
    public String sumaDeByte(String numByteUno, String numByteDos){
        String resultado ="los número no son los corretos";      
        if(numByteUno.length() <= 8 && numByteDos.length() <=8){            
           resultado = sumaDeBinarios(numByteUno, numByteDos);
           if(resultado.length() > 8){
               resultado = "Hubo desvordamiento de pila los valores dentro del rango de un byte son: "+resultado.substring(0,8);
            
            }        
        }
        return resultado;
    }
    public String sumaDeByteWord(String numByte, String numWord){
        String resultado ="los número no son los corretos";        
        if(numByte.length() <= 8 && numWord.length() <=16){            
           resultado =  sumaDeBinarios(numByte,numWord);
           if(resultado.length() > 16){
               resultado = "Hubo desvordamiento de pila los valores dentro del rango de un Word son: "+resultado.substring(0,16);
            }       
    }
        return resultado;
    }
    public String sumaDeByteDWord(String numByte, String numDWord){
        String resultado ="los número no son los corretos";        
        if(numByte.length() <= 8 && numDWord.length() <=32){            
           resultado =  sumaDeBinarios(numByte,numDWord);
           if(resultado.length() > 32){
                resultado = "Hubo desvordamiento de pila los valores dentro del rango de un DWord son: "+resultado.substring(0,32);
            }
        }
        return resultado;
    }
      
      public String sumaDeByteQWord(String numByte, String numQWord){
        String resultado ="los número no son los corretos";       
        if(numByte.length() <= 8 && numQWord.length() <=64){            
           resultado =  sumaDeBinarios(numByte,numQWord);
           if(resultado.length() > 64){
                resultado = "Hubo desvordamiento de pila los valores dentro del rango de un DWord son: "+resultado.substring(0,64);
            }
       
    }
        return resultado;
    }
    
    public void escojerConversion(){
        Scanner sn = new Scanner(System.in);
        System.out.println("Suma de Byte");
        System.out.println("Puedes sumar dos byte, y como resultado tener:"
                          +"\n1.- un Byte"
                          +"\n2.- un Word"
                          +"\n3.- un DWord"
                          +"\n4.- un Qword"
                          +"\n Ingresa el número del tipo de dato, en el que quieres tu resultado");
        try{
              int num = Integer.parseInt(sn.nextLine());
        System.out.println("Ingresa el primer número en Byte 8 bits");
        String numUno = sn.nextLine();
        System.out.println("Ingresa el segundo número en Byte 8 bits");
        String numDos = sn.nextLine();
        String resultado="";
        switch(num){
            case 1: resultado = sumaDeByte(numUno, numDos);
                    break;
            case 2: resultado = sumaDeByteWord(numUno, numDos);
                    break;
            case 3: resultado = sumaDeByteDWord(numUno, numDos);
                    break;
            case 4: resultado = sumaDeByteQWord(numUno, numDos);
        }
        System.out.println("La suma en "+tiposDeDatos[num-1]+" es = "+resultado);
        }catch(Exception e){
            System.out.println("Porfavor de ingresar números del 1 al 4 para el memu y los números byte solo son 1 y 0 :-)");
        }
      
    }  
public static void main(String[] agrs){
    TipoByte uno = new TipoByte();     
    uno.escojerConversion();
}
}
