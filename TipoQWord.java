/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lesm.tamanioAlmacenamiento;

import java.util.Scanner;

/**
 *
 * @author Luis
 */
public class TipoQWord {
     private static final String[] tiposDeDatos = {"QWord"};
      public String sumaDeBinarios(String numBinario1, String numBinario2){
        String[] arr;
        arr = (numBinario1.length() >= numBinario2.length()) ? new String[numBinario1.length()+1]: new String[numBinario2.length()+1];
        int n = 0;       
        String resultado = "";
        String acarreo = "0";
        String num1 = "";
        String num2 = "";
         int longitudUno = numBinario1.length();
         int longitudDos = numBinario2.length();
        while(longitudUno >= 0 || longitudDos >= 0){         
            if(longitudUno > 0){
                try{
                  num1 = numBinario1.substring((longitudUno-1),longitudUno);                 
                }catch(Exception e){}                 
            }else{num1 = "0"; }
            longitudUno--;
            if(longitudDos >0){
                try{
                num2 = numBinario2.substring((longitudDos-1), longitudDos);
                }catch(Exception ee){}                 
            }else{num2 = "0"; } 
            longitudDos--;
            if(num1.equals(num2) && num1.equals("1")){                             
                if(acarreo.equals("1")){                  
                   arr[n] = "1";
                   acarreo = "1";
                }else{arr[n] = "0"; acarreo = "1";}    
            }else if(num1.equals(num2) && num1.equals("0")){               
                if(acarreo.equals("1")){                   
                   arr[n]="1";
                   acarreo = "0";
                }else{arr[n]="0";}                
            }else{                 
                if(acarreo.equals("1")){                  
                    arr[n]="0";
                   acarreo = "1";
                }else{arr[n]="1";}                
             }           
            n++;
        }             
        for(int x = arr.length-1; x >= 0; x--){            
                resultado += arr[x];
        }
        return resultado;
    }
      
      public String sumaDeQWord(String numQWordUno, String numQWordDos){
        String resultado ="los número no son los corretos";      
        if(numQWordUno.length() <= 64 && numQWordDos.length() <=64){            
           resultado = sumaDeBinarios(numQWordUno, numQWordDos);
           if(resultado.length() > 64){
              resultado = "Hubo desvordamiento de pila los valores dentro del rango de un QWord son: "+resultado.substring(0,64);
            }
        
        }
        return resultado;
    }
      
      public void escojerConversion(){
        Scanner sn = new Scanner(System.in);
        System.out.println("Suma de QWord");
        System.out.println("Puedes sumar dos QWord, y como resultado tener:"                          
                          +"\n1.- un Qword"
                          +"\n Ingresa el número 1 para continuar");     
        try{
            int num = Integer.parseInt(sn.nextLine());
        System.out.println("Ingresa el primer número en QWord 64 bits");
        String numUno = sn.nextLine();
        System.out.println("Ingresa el segundo número en QWord 64 bits");
        String numDos = sn.nextLine();
        String resultado="";
        switch(num){
            case 1: resultado = sumaDeQWord(numUno, numDos);
        }
         System.out.println("La suma en "+tiposDeDatos[num-1]+" es = "+resultado);
        }catch(Exception e){
              System.out.println("Porfavor de ingrese el número 1 y pulsar la tecla enter y los números Qword solo son 1 y 0 :-)");
        }
        
    } 
      public static void main(String[] agrs){
            TipoQWord uno = new TipoQWord();
            uno.escojerConversion();
            
            
}
}
